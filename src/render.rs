use super::{
    Action, AppState, ControlKind, EnterPosition, GameData, ResultChange, ResultChangeDirection,
    ResultChangeKind,
};
use mastermindsolve::{Colour, GuessRow};
use seed::virtual_dom::node::Node;
use seed::{prelude::*, *};
use std::borrow::Cow;

#[derive(Clone)]
pub enum ChosenMode {
    Solver,
    AgainstComputer,
    AgainstPlayer,
}

pub fn render(model: &AppState) -> impl View<Action> {
    let mut root = div![];

    root.add_child(match model {
        AppState::MainMenu { colour_amount } => render_main_menu(&MainMenuData {
            colour_amount: *colour_amount,
        }),
        AppState::InGame(game_data) => render_in_game(game_data),
    });

    root
}

struct MainMenuData {
    colour_amount: u8,
}

fn render_main_menu(data: &MainMenuData) -> Node<Action> {
    div![
        attrs! {At::Class => "main-menu"},
        mode_selection(),
        switch_colour_amount(data.colour_amount),
    ]
}

fn mode_selection() -> Node<Action> {
    div![
        attrs! {At::Class => "mode-selection"},
        button![
            attrs! {At::Class => "select-against-computer"},
            simple_ev(Ev::Click, Action::ChooseMode(ChosenMode::AgainstComputer)),
            "Against Computer",
        ],
        button![
            attrs! {At::Class => "select-against-player"},
            simple_ev(Ev::Click, Action::ChooseMode(ChosenMode::AgainstPlayer)),
            "Against Player",
        ],
        button![
            attrs! {At::Class => "select-solver"},
            simple_ev(Ev::Click, Action::ChooseMode(ChosenMode::Solver)),
            "Solver",
        ],
    ]
}

fn switch_colour_amount(current: u8) -> Node<Action> {
    let current_div = div![format!("Currently in {} Colour Mode", current)];
    let other_color = if current == 6 {8} else {6};

    div![
        attrs!{At::Class => "switch-colour-amount"},
        current_div,
        button![simple_ev(Ev::Click, Action::SetColourAmount(other_color)), "Switch"],
    ]
}

fn render_in_game(game_data: &GameData) -> Node<Action> {
    let mut in_game = div![
        attrs! {At::Class => "in-game"},
        render_game_board(game_data),
        render_control_row(game_data),
    ];

    if let Some(solutions) = game_data.current_solutions() {
        in_game.add_child(div![attrs!{At::Class => "solution-header"},"Solutions"]);
        in_game.add_child(show_solutions(solutions));
    }

    in_game
}

fn render_control_row(game_data: &GameData) -> Node<Action> {
    div![
        attrs! {At::Class => "control-row"},
        match game_data.control_kind() {
            ControlKind::Colours => render_colour_selection(game_data.colour_amount),
            ControlKind::Result => result_change_controls(game_data),
        },
        row_control(game_data),
    ]
}

fn render_game_board(game_data: &GameData) -> Node<Action> {
    let mut rendered = div![attrs! {At::Class => "gameboard"},];

    for index in 0..game_data.rows().len() {
        rendered.add_child(render_row(index, game_data));
    }

    rendered
}

fn render_row(row_index: usize, game_data: &GameData) -> Node<Action> {
    let row = &game_data.rows()[row_index];
    let mut slots = div![attrs! {At::Class => "slots"},];
    {
        let mut guess_slots = div![attrs! {At::Class => "guess-slots"},];

        row.guessed_colors
            .iter()
            .enumerate()
            .map(|(index, colour)| {
                let enter_position = EnterPosition::of_guess(row_index, index as u8);
                let is_selected = enter_position.is_equal(&game_data.enter_position);
                let enter_enabled = game_data.enter_enabled(&enter_position);
                render_slot(
                    colour,
                    &format!("guess-slot-{}", index),
                    enter_position,
                    is_selected,
                    enter_enabled,
                )
            })
            .for_each(|(slot, button)| {
                guess_slots.add_child(slot);
                if let Some(elem) = button {
                    guess_slots.add_child(elem);
                }
            });

        slots.add_child(guess_slots);
    }

    slots.add_child(render_results(row_index, game_data));

    slots
}

fn render_results(row_index: usize, game_data: &GameData) -> Node<Action> {
    let row = &game_data.rows()[row_index];
    let result = &row.result;
    let mut result_slot = 0;
    let enter_position = EnterPosition::of_solution(row_index);
    let is_selected = enter_position.is_equal(&game_data.enter_position);
    let mut result_slots = div![
        attrs! {At::Class => "result-slots",  At::Custom(Cow::Borrowed("has-focus")) => if is_selected {"true"} else {"false"},},
    ];

    if game_data.enter_enabled(&enter_position) {
        result_slots.add_child(button![
            attrs! {
                At::Class => "focus-result-button",
            },
            simple_ev(Ev::Click, Action::ChangeSlot(enter_position)),
        ]);
    }

    for _num in 0..result.correct_colors {
        result_slots.add_child(div![attrs! {
            At::Style => format!("background-color: {};", to_rgb(white_colour())),
            At::Class => format!("result-slot-{}", result_slot),
        },]);
        result_slot += 1;
    }

    for _num in 0..result.correct_positions {
        result_slots.add_child(div![attrs! {
            At::Style => format!("background-color: {};", to_rgb(black_colour())),
            At::Class => format!("result-slot-{}", result_slot),
        },]);
        result_slot += 1;
    }
    // Add empty slots
    for _ in result_slot .. 4 {
        result_slots.add_child(div![attrs! {
            At::Style => "background-color: #0000;",
            At::Class => format!("result-slot-{} empty-slot", result_slot),
        },]);
        result_slot += 1;
    }

    result_slots
}

fn render_slot(
    colour: &Option<Colour>,
    slot_class: &str,
    position: EnterPosition,
    is_enter: bool,
    enter_enabled: bool,
) -> (Node<Action>, Option<Node<Action>>) {
    let additional_class = if colour.is_none() {"empty-slot"} else {""};
    let colour = colour.map(to_rgb).unwrap_or("#0000");
    let div = div![attrs! {
        At::Style => format!("background-color: {};", colour),
        At::Class => format!("{} {}", slot_class, additional_class),
        At::Custom(Cow::Borrowed("has-focus")) => if is_enter {"true"} else {"false"},
    },];
    if enter_enabled {
        (div, Some(button![
            attrs! {
                At::Class => format!("slot-button {}", slot_class),
            },
            simple_ev(Ev::Click, Action::ChangeSlot(position)),
        ]))
    } else {
        (div, None)
    }
}

fn black_colour() -> Colour {
    Colour::new(7)
}

fn white_colour() -> Colour {
    Colour::new(6)
}

fn render_colour_selection(colour_amount: u8) -> Node<Action> {
    colour_selection(&gen_colours(colour_amount))
}

fn gen_colours(colour_amount: u8) -> Vec<Colour> {
    (0..colour_amount).map(Colour::new).collect()
}

fn colour_selection(colours: &[Colour]) -> Node<Action> {
    let mut div = div![attrs! {At::Class => "colour-selection"}];

    for colour in colours {
        div.add_child(button![
            attrs! {
                At::Class => "colour-choose-button",
                At::Style => format!("background-color: {};", to_rgb(*colour))
            },
            simple_ev(Ev::Click, Action::ChooseColor(*colour)),
        ]);
    }

    div
}

fn row_control(game_data: &GameData) -> Node<Action> {
    let mut commit_row_attrs = attrs! {At::Class => "commit-row"};
    if !game_data.commit_row_enabled() {
        commit_row_attrs.add(At::Disabled, "true");
    }

    let mut undo_row_attrs = attrs! {At::Class => "undo-row"};
    if !game_data.undo_row_enabled() {
        undo_row_attrs.add(At::Disabled, "true");
    }

    div![
        attrs! {At::Class => "row-control-buttons"},
        button![
            commit_row_attrs,
            simple_ev(Ev::Click, Action::CommitRow),
            "Commit"
        ],
        button![
            undo_row_attrs,
            simple_ev(Ev::Click, Action::UndoRow),
            "Undo"
        ],
    ]
}

fn result_change_controls(game_data: &GameData) -> Node<Action> {
    use ResultChangeDirection::*;
    use ResultChangeKind::*;

    div![
        attrs! {At::Class => "result-change-control"},
        result_change_control_button(game_data, More, RightColours),
        result_change_control_button(game_data, More, RightPositions),
        result_change_control_button(game_data, Less, RightColours),
        result_change_control_button(game_data, Less, RightPositions)
    ]
}

fn result_change_control_button(
    game_data: &GameData,
    direction: ResultChangeDirection,
    kind: ResultChangeKind,
) -> Node<Action> {
    use ResultChangeDirection::*;
    use ResultChangeKind::*;

    let (class, text) = match (direction, kind) {
        (More, RightColours) => ("more-right-colours", "+ White"),
        (More, RightPositions) => ("more-right-positions", "+ Black"),
        (Less, RightColours) => ("less-right-colours", "- White"),
        (Less, RightPositions) => ("less-right-positions", "- Black"),
    };

    let attributs = if !game_data.can_change_result(ResultChange::new(kind, direction)) {
        attrs! {At::Class => class, At::Disabled => "true"}
    } else {
        attrs! {At::Class => class}
    };

    button![
        attributs,
        simple_ev(
            Ev::Click,
            Action::ResultChange(ResultChange::new(kind, direction))
        ),
        text
    ]
}

fn show_solutions(solutions: &[GuessRow]) -> Node<Action> {
    let mut div = div![attrs! {At::Class => "solutions"}];

    if solutions.len() > 100 {
        return div;
    }

    solutions
        .iter()
        .map(GuessRow::colours)
        .map(|colours| colours.iter())
        .map(|colours| {
            let mut color_div = div![attrs! {At::Class => "solution"}];
            colours
                .map(|colour| {
                    div![attrs! {
                        At::Style => format!("background-color: {};", to_rgb(*colour)),
                    }]
                })
                .for_each(|elem| {
                    color_div.add_child(elem);
                });
            color_div
        })
        .for_each(|row| {
            div.add_child(row);
        });

    div
}

fn to_rgb(colour: Colour) -> &'static str {
    let colors = [
        "#BB0707", "#44a402", "#0F0FCE", "#DBDB14", "#957a58", "#FF5400", "#FFF", "#000",
    ];

    if colour.number() as usize <= colors.len() {
        colors[colour.number() as usize]
    } else {
        "#000"
    }
}
