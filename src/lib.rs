use mastermindsolve::{Colour, Guess, GuessRow, RowComparision};
use render::ChosenMode;
use seed::prelude::*;
use std::convert::TryInto;

mod render;

pub enum AppState {
    MainMenu { colour_amount: u8 },
    InGame(GameData),
}

impl Default for AppState {
    fn default() -> Self {
        AppState::MainMenu { colour_amount: 6 }
    }
}

impl AppState {
    fn to_main_menu(&mut self) {
        match self {
            AppState::MainMenu { .. } => (),
            AppState::InGame(data) => {
                *self = AppState::MainMenu {
                    colour_amount: data.colour_amount,
                };
            }
        }
    }
}

pub struct GameData {
    mode: GameMode,
    rows: [BoardRow; 8],
    colour_amount: u8,
    pub enter_position: Option<EnterPosition>,
}

impl GameData {
    fn new_solver(colour_amount: u8) -> Self {
        Self {
            rows: Default::default(),
            mode: GameMode::Solver {
                solutions: Vec::new(),
            },
            colour_amount,
            enter_position: Some(EnterPosition::of_guess(0, 0)),
        }
    }

    fn new_against_computer(colour_amount: u8) -> Self {
        Self {
            rows: Default::default(),
            mode: GameMode::AgainstComputer {
                solution: random_solution(colour_amount),
                rows_entered: 0,
            },
            colour_amount,
            enter_position: Some(EnterPosition::of_guess(0, 0)),
        }
    }

    fn new_against_player(colour_amount: u8) -> Self {
        Self {
            rows: Default::default(),
            mode: GameMode::AgainstPlayer,
            colour_amount,
            enter_position: Some(EnterPosition::of_guess(0, 0)),
        }
    }

    pub fn rows(&self) -> &[BoardRow; 8] {
        &self.rows
    }

    pub fn enter_enabled(&self, enter_position: &EnterPosition) -> bool {
        match self.mode {
            GameMode::AgainstPlayer { .. } | GameMode::Solver { .. } => true,
            GameMode::AgainstComputer { rows_entered, .. } if rows_entered != enter_position.row_index => {
                false
            }
            GameMode::AgainstComputer { .. } => match enter_position.slot {
                EnterSlot::GuessSlot(_) => true,
                EnterSlot::SolutionSlot => false,
            },
        }
    }

    pub fn commit_row_enabled(&self) -> bool {
        match self.mode {
            GameMode::AgainstComputer { rows_entered, .. } => rows_entered < 8 && self.rows[rows_entered].guesses_complete(),
            GameMode::AgainstPlayer { .. } | GameMode::Solver { .. } => false,
        }
    }

    pub fn commit_row(&mut self) {
        match &mut self.mode {
            GameMode::AgainstPlayer { .. } | GameMode::Solver { .. } => (),
            GameMode::AgainstComputer {
                rows_entered,
                solution,
            } => {
                self.rows[*rows_entered].create_result(solution);
                *rows_entered += 1;
                self.enter_position = if *rows_entered < 8 {
                    Some(EnterPosition::of_guess(*rows_entered, 0))
                } else {
                    None
                }
            }
        }

    }

    pub fn undo_row_enabled(&self) -> bool {
        match self.mode {
            GameMode::AgainstComputer { rows_entered, .. } => rows_entered > 0,
            GameMode::AgainstPlayer { .. } | GameMode::Solver { .. } => false,
        }
    }

    pub fn undo_row(&mut self) {
        match &mut self.mode {
            GameMode::AgainstComputer { rows_entered, .. } => {
                if *rows_entered < 8 {
                    self.rows[*rows_entered].clear();
                }
                *rows_entered -= 1;
                self.rows[*rows_entered].clear_result();
                self.enter_position = None;
            }
            GameMode::AgainstPlayer { .. } | GameMode::Solver { .. } => (),
        }
    }

    pub fn control_kind(&self) -> ControlKind {
        match self.mode {
            GameMode::Solver { .. } | GameMode::AgainstPlayer { .. } => match &self.enter_position {
                Some(enter_position) => match enter_position.slot {
                    EnterSlot::GuessSlot(_) => ControlKind::Colours,
                    EnterSlot::SolutionSlot => ControlKind::Result,
                },
                None => ControlKind::Colours,
            },
            GameMode::AgainstComputer { .. } => ControlKind::Colours,
        }
    }

    fn possible_solutions_for_valid_rows(rows: &[BoardRow], colour_amount: u8) -> Vec<GuessRow> {
        let guesses: Vec<Guess> = rows
            .iter()
            .filter(|row| row.guesses_complete())
            .map(|guess_row| guess_row.try_into().unwrap())
            .collect();

        mastermindsolve::find_solutions(&guesses[..], colour_amount).collect()
    }

    pub fn current_solutions(&self) -> Option<&Vec<GuessRow>> {
        match &self.mode {
            GameMode::Solver { solutions } => Some(solutions),
            _ => None,
        }
    }

    pub fn can_change_result(&self, change: ResultChange) -> bool {
        let current_result = if let Some(enter_position) = &self.enter_position {
            &self.rows[enter_position.row_index].result
        } else {
            return false;
        };
        let space_left = current_result.correct_colors + current_result.correct_positions < 4;
        use ResultChangeDirection::*;
        use ResultChangeKind::*;
        match (change.direction, change.kind) {
            (More, RightColours) => current_result.correct_colors < 4 && space_left,
            (Less, RightColours) => current_result.correct_colors > 0,
            (More, RightPositions) => current_result.correct_positions < 4 && space_left,
            (Less, RightPositions) => current_result.correct_positions > 0,
        }
    }
}

pub enum ControlKind {
    Colours,
    Result,
}

//Gamelogic

impl GameData {
    fn update_solutions(&mut self) {
        if let GameMode::Solver { solutions } = &mut self.mode {
            *solutions = Self::possible_solutions_for_valid_rows(&self.rows, self.colour_amount);
        }
    }

    fn choose_colour(&mut self, colour: Colour) {
        if let Some(position) = self.enter_position.clone() {
            match position.slot {
                EnterSlot::GuessSlot(slot) => {
                    self.rows[position.row_index].guessed_colors[slot as usize] = Some(colour);
                    self.update_solutions();
                    self.enter_position = self.next_selection(&position);
                }
                _ => (),
            }
        }
    }

    fn change_result(&mut self, result_change: ResultChange) {
        if let Some(enter_position) = &self.enter_position {
            result_change.apply(&mut self.rows[enter_position.row_index].result);
            self.update_solutions();
        }
    }

    fn next_selection(&self, pos: &EnterPosition) -> Option<EnterPosition> {
        match pos.slot {
            EnterSlot::GuessSlot(index)  => {
                if let Some(unfilled) = self.rows[pos.row_index].next_unfilled_color(index) {
                    return Some(EnterPosition::of_guess(pos.row_index, unfilled));
                } else {
                     match self.mode {
                        GameMode::AgainstPlayer { .. } | GameMode::Solver { .. } => {
                            Some(EnterPosition::of_solution(pos.row_index))
                            },
                        GameMode::AgainstComputer { .. } => None,
                    }
                }
            }
            _ => None,
        }
    }
}

#[derive(Clone, Copy)]
pub struct ResultChange {
    kind: ResultChangeKind,
    direction: ResultChangeDirection,
}

impl ResultChange {
    pub fn new(kind: ResultChangeKind, direction: ResultChangeDirection) -> Self {
        Self { kind, direction }
    }

    fn apply(&self, result: &mut GuessRowResult) {
        use ResultChangeDirection::*;
        use ResultChangeKind::*;
        let to_change = match self.kind {
            RightColours => &mut result.correct_colors,
            RightPositions => &mut result.correct_positions,
        };

        match self.direction {
            More => *to_change += 1,
            Less => *to_change -= 1,
        };
    }
}

#[derive(Clone, Copy)]
pub enum ResultChangeKind {
    RightColours,
    RightPositions,
}

#[derive(Clone, Copy)]
pub enum ResultChangeDirection {
    More,
    Less,
}

fn random_solution(max_color: u8) -> [Colour; 4] {
    use wbg_rand::{Rng, wasm_rng};
    let mut nums = Vec::with_capacity(4);
    let mut rng = wasm_rng();
    while nums.len() < 4 {
        let num = rng.gen::<u8>() % max_color;
        if !nums.contains(&num) {
            nums.push(num);
        }
    }
    [
        Colour::new(nums[0]),
        Colour::new(nums[1]),
        Colour::new(nums[2]),
        Colour::new(nums[3]),
    ]
}

#[derive(Clone, PartialEq, Eq)]
pub struct EnterPosition {
    pub row_index: usize,
    pub slot: EnterSlot,
}

impl EnterPosition {
    pub fn of_guess(row_index: usize, slot: u8) -> Self {
        Self {
            row_index,
            slot: EnterSlot::GuessSlot(slot),
        }
    }

    pub fn of_solution(row_index: usize) -> Self {
        Self {
            row_index,
            slot: EnterSlot::SolutionSlot,
        }
    }

    pub fn is_equal(&self, other: &Option<Self>) -> bool {
        other.as_ref().map(|pos| *pos == *self).unwrap_or(false)
    }
}

#[derive(Clone, PartialEq, Eq)]
pub enum EnterSlot {
    GuessSlot(u8),
    SolutionSlot,
}

enum GameMode {
    Solver {
        solutions: Vec<GuessRow>,
    },
    AgainstComputer {
        solution: [Colour; 4],
        rows_entered: usize,
    },
    AgainstPlayer,
}

struct GuessRowResult {
    correct_colors: u8,
    correct_positions: u8,
}

impl From<&RowComparision> for GuessRowResult {
    fn from(comparision: &RowComparision) -> Self {
        Self {
            correct_colors: comparision.colors_in_wrong_position(),
            correct_positions: comparision.same_positions(),
        }
    }
}

pub struct BoardRow {
    guessed_colors: [Option<Colour>; 4],
    result: GuessRowResult,
}

impl Default for BoardRow {
    fn default() -> Self {
        Self {
            guessed_colors: [None; 4],
            result: GuessRowResult {
                correct_colors: 0,
                correct_positions: 0,
            },
        }
    }
}
impl BoardRow {
    fn guesses_complete(&self) -> bool {
        self.guessed_colors.iter().all(Option::is_some)
    }

    fn create_result(&mut self, solution: &[Colour; 4]) {
        let guessrow = GuessRow::new([
            self.guessed_colors[0].unwrap(),
            self.guessed_colors[1].unwrap(),
            self.guessed_colors[2].unwrap(),
            self.guessed_colors[3].unwrap(),
        ]);

        let solution = GuessRow::new(*solution);

        self.result = GuessRowResult::from(&guessrow.compare(&solution));
    }

    fn clear(&mut self) {
        *self = Default::default();
    }

    fn clear_result(&mut self) {
        let def_board: BoardRow = Default::default();
        self.result = def_board.result;
    }

    fn next_unfilled_color(&self, start: u8) -> Option<u8> {
        for index in (start as usize .. self.guessed_colors.len()).chain(0..start as usize) {
            if let None = self.guessed_colors[index] {
                return Some(index as u8);
            }
        }
        None
    }
}

impl TryInto<Guess> for &BoardRow {
    type Error = &'static str;

    fn try_into(self) -> Result<Guess, &'static str> {
        match self.guessed_colors {
            [Some(colour0), Some(colour1), Some(colour2), Some(colour3)] => Ok(Guess::new(
                self.result.correct_colors,
                self.result.correct_positions,
                [colour0, colour1, colour2, colour3],
            )),
            _ => Err("Not all Colours are guessed"),
        }
    }
}

#[derive(Clone)]
pub enum Action {
    ToMainMenu,
    SetColourAmount(u8),
    ChooseMode(ChosenMode),
    ChooseColor(Colour),
    ChangeSlot(EnterPosition),
    CommitRow,
    UndoRow,
    ResultChange(ResultChange),
}

fn update(action: Action, model: &mut AppState, _: &mut impl Orders<Action>) {
    match model {
        AppState::MainMenu { colour_amount } => match action {
            Action::SetColourAmount(amount) => *colour_amount = amount,
            Action::ChooseMode(mode) => {
                let game_data = match mode {
                    ChosenMode::AgainstComputer => GameData::new_against_computer(*colour_amount),
                    ChosenMode::Solver => GameData::new_solver(*colour_amount),
                    ChosenMode::AgainstPlayer => GameData::new_against_player(*colour_amount),
                };
                *model = AppState::InGame(game_data);
            }
            Action::ToMainMenu => (),
            Action::ChooseColor(_)
            | Action::ChangeSlot(_)
            | Action::CommitRow
            | Action::UndoRow
            | Action::ResultChange(_) => unreachable!(),
        },
        AppState::InGame(game_data) => match action {
            Action::ToMainMenu => model.to_main_menu(),
            Action::ChangeSlot(enter_position) => game_data.enter_position = Some(enter_position),
            Action::ChooseColor(colour) => game_data.choose_colour(colour),
            Action::CommitRow => game_data.commit_row(),
            Action::UndoRow => game_data.undo_row(),
            Action::ResultChange(result_change) => game_data.change_result(result_change),
            Action::ChooseMode(_) | Action::SetColourAmount(_) => unreachable!(),
        },
    };
}

#[wasm_bindgen(start)]
pub fn render() {
    App::builder(update, render::render).build_and_start();
}
